# README #

Buzz Eat Love replaces cafe and restaurant buzzers with a user's personal phone by sending SMS notifications when a meal is ready.

This repository contains the source code (minus API keys) for the Buzz Eat Love web app, found at www.buzzeatlove.com. A demo video can be found at http://youtu.be/gQnN0XU2qbU

This project is based on Google App Engine.

By Luke Marshall, 2015