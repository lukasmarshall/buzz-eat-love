from handlers.handler import Handler
from templates.templates import Templates
from google.appengine.api import users
import models.userData as userData

class PreferencesHandler(Handler):
	def on_post(self):
		user = users.get_current_user()
		custom_message = self.request.get('custom_message', None)
		if user and custom_message:
			user_data = userData.getUserData(user)
			user_data.setCustomMessage(custom_message)
		else:
			print "User Not Found"
			self.redirect(users.create_login_url(self.request.uri))


