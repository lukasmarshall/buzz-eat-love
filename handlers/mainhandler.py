from handlers.handler import Handler
from templates.templates import Templates

class MainHandler(Handler):
	def on_get(self):
		data = self.get_data()
		template = Templates.load('index.html', data)
		self.response.write(template)