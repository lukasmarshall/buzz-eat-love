from handlers.handler import Handler
from templates.templates import Templates
from google.appengine.api import users
import models.userData as userData

class DashboardHandler(Handler):
	def on_get(self):
		user = users.get_current_user()

		if user:
			print "Returning Dashboard Page"
			user_data = userData.getUserData(user) #by calling getUserData we create a new user object if none existed
			data = {'user_data':user_data,
					'turnaround_time':user_data.getTurnAroundTime(),
					'logout_url':users.create_logout_url('/logoutSuccess')}
			template = Templates.load('dashboard.html', data)
			self.response.write(template)
		else:
			print "User Not Found"
			self.redirect(users.create_login_url(self.request.uri))
