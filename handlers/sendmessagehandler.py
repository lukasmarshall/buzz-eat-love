
from handlers.handler import Handler
import models.token as token
# from templates.templates import Templates
from google.appengine.api import users
import urllib2
import urllib
from TelstraSMSAUSLib.Controllers.APIController import *
import models.userData as userData
import models.order as orderModel




class SendMessageHandler(Handler):
	def on_post(self):
		user = users.get_current_user()
		mobile_number = self.request.get('mobile_number', None)
		unique_order_id = self.request.get('unique_order_id', None)
		if user and mobile_number and unique_order_id:
			sendMessage(user, mobile_number, unique_order_id)
		else:
			print "Message Send Error: Not enough information. user: "+str(user)+" mobile_number: "+str(mobile_number)+" unique_order_id: "+str(unique_order_id)

def sendMessage(user, mobile_number, unique_order_id):
		order_object = orderModel.getOrder(user, mobile_number, unique_order_id)
		order_object.setMessageSent()

		sendSMS(mobile_number, user)
		
		user_data = userData.getUserData(user)
		user_data.incrementSentMessageCounters()


def sendSMS(mobile_number, user):
	print "sending message"
	controller = APIController()
	client_id = "eKQjt1A2SHyNzBfDDCPdTuNxJnVAQEa8"
	client_secret = "a0suBWDAWiqAiGqi"
	user_data = userData.getUserData(user)
	auth = controller.get_authentication({'client_id': client_id, 'client_secret': client_secret, 'grant_type':'client_credentials','scope':'SMS'})
	print auth.resolve_names()
	print mobile_number
	response = controller.create_sms_message({'to':str(mobile_number), 'body':user_data.getUnescapedMessage()})

	user_data = userData.getUserData(user)


