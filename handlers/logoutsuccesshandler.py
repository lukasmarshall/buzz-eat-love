from handlers.handler import Handler
from templates.templates import Templates

class LogoutSuccessHandler(Handler):
	def on_get(self):
		data = self.get_data()
		template = Templates.load('logoutSuccess.html', data)
		self.response.write(template)