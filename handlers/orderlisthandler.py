from handlers.handler import Handler
from templates.templates import Templates
from models.order import Order
import models.order
from google.appengine.api import users

class OrderListHandler(Handler):
	def on_get(self):
		user = users.get_current_user()
		if user:
			orders = getOrders(user)
			template_values = {'orders':orders}
			template = Templates.load('order_list.html', template_values)
			self.response.write(template)


def getOrders(user):
	ordersQuery = Order.query(Order.order_finished == False, ancestor=models.order.order_key(user.email())).order(-Order.date)
	orders = ordersQuery.fetch()
	return orders