from handlers.handler import Handler
# from templates.templates import Templates

import models.order 
from google.appengine.api import users
import datetime
import models.userData as userData
import stripe
import handlers.stripeKeys as stripeKeys


class PaymentHandler(Handler):
	def on_post(self):
		print "Processing Payment"
		# Get the credit card details submitted by the form
		token = self.request.get('stripeToken', None)
		# Get the current user
		user = users.get_current_user()
		if user and token:
			newPayment(user = user, token = token)
		

def newPayment(user, token):
	# Set your secret key: remember to change this to your live secret key in production
	# See your keys here https://dashboard.stripe.com/account/apikeys
	stripe.api_key = stripeKeys.getLiveSecretKey()

	# Create a Customer in Stripe
	customer = stripe.Customer.create(
	    source=token,
	    description="Example customer"
	)

	# Save the customer ID in your database so you can use it later
	save_stripe_customer_id(user, customer.id)

	# Later...
	customer_id = get_stripe_customer_id(user)

	try:
		# Create the charge on Stripe's servers - this will charge the user's card
		stripe.Charge.create(
		    amount=1000, 
		    currency="aud",
		    customer=customer_id
		)
		user_data = userData.getUserData(user)

		user_data.addMessages(1000)
	

	
	except stripe.error.CardError, e:
	  # The card has been declined
	  print "Card was declined :<"
	  pass

def save_stripe_customer_id(user, customer_id):
	user_data = userData.getUserData(user)
	user_data.setStripeCustomerID(customer_id)

def get_stripe_customer_id(user):
	user_data = userData.getUserData(user)
	return user_data.getStripeCustomerID()







