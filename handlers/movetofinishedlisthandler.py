from handlers.handler import Handler
from google.appengine.api import users
import models.order as order

class MoveToFinishedListHandler(Handler):
	def on_post(self):
		user = users.get_current_user()
		mobile_number = int(self.request.get('mobile_number', None))
		unique_order_id = int(self.request.get('unique_order_id', None))
		if user and mobile_number and unique_order_id:
			print "Moving to finished list by setting message to 'sent' "
			order_object = order.getOrder(user, mobile_number, unique_order_id)
			order_object.setOrderFinished()
		else:
			print "Not enough info to move to finished list"
