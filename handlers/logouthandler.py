from handlers.handler import Handler
from templates.templates import Templates
from google.appengine.api import users
import models.userData as userData

class LogoutHandler(Handler):
	def on_get(self):
		self.redirect(users.create_logout_url(self.request.uri))

		