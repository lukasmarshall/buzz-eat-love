from handlers.handler import Handler
from templates.templates import Templates
from models.order import Order
import models.order
from google.appengine.api import users
from google.appengine.ext import ndb
import models.userData as userData

class ResetOrderCounterHandler(Handler):
	def on_post(self):
		user = users.get_current_user()

		if user:
			user_data = userData.getUserData(user) #by calling getUserData we create a new user object if none existed
			user_data.resetStats() #reset the user's stats
			data = {'user_data':user_data,
					'turnaround_time':user_data.getTurnAroundTime()}
			template = Templates.load('stats_table.html', data)
			self.response.write(template)
		else:
			print "User Not Found"
			self.redirect(users.create_login_url(self.request.uri))

