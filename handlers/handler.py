from webapp2 import RequestHandler
from google.appengine.ext import ndb

class Handler(RequestHandler):
	_data = None

	@ndb.toplevel
	def get(self, *args, **kwargs):
		data = self._build_data(*args, **kwargs)
		if data is not None:
			self._add_data(data)
			self.on_get()

	@ndb.toplevel
	def post(self, *args, **kwargs):
		data = self._build_data(*args, **kwargs)
		if data is not None:
			self._add_data(data)
			self.on_post()

	@ndb.toplevel
	def put(self, *args, **kwargs):
		data = self._build_data(*args, **kwargs)
		if data is not None:
			self._add_data(data)
			self.on_put()

	@ndb.toplevel
	def delete(self, *args, **kwargs):
		data = self._build_data(*args, **kwargs)
		if data is not None:
			self._add_data(data)
			self.on_delete()

	def _build_data(self, *args, **kwargs):
		try:
			data = {}
			args = self.request.arguments()
			args = { arg: self.request.get(arg) for arg in args }
			data.update(args)

			data.update(**kwargs)
			return data
		except Exception as e:
			self.response.status = 400

	def _add_data(self, data):
		if self._data is None:
			self._data = {}
		self._data.update(data)
		return self._data

	def get_data(self):
		return self._data

	def on_get(self):
		pass

	def on_post(self):
		pass

	def on_put(self):
		pass

	def on_delete(self):
		pass