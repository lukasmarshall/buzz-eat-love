#!/bin/sh

#MODULES
modules=("module1" "module2")

#DECLARATIONS
id=$(jq -r '.APP_ID' $1)

yaml=()
yaml_out=()

for var in "${modules[@]}"
do
	yaml=(${yaml[@]} "${var}/${var}.yaml")
	yaml_out=(${yaml_out[@]} "${var}/${var}-injected.yaml")
done

#INJECT SECRETS INTO YAML
add_secrets () {
	injected=`cat $2`
	injected="$injected"$'\n'$'\n'"env_variables:"

	secrets=$(jq -r 'to_entries | .[] | .[]' $1)
	while read key
		do
			read val
			injected="$injected"$'\n'"  $key: $val"
		done <<< "$secrets"
}

clear_shared () {
	for var in "${modules[@]}"
	do
		rm -rf "./${var}/shared/"
	done
}

copy_shared () {
	clear_shared
	for var in "${modules[@]}"
	do
		cp -r ./shared "./${var}/"
	done
}

i=0
for each in "${yaml[@]}"
do
	add_secrets $1 $each
	out_file=${yaml_out[$i]}
	echo "$injected" | cat > $out_file
	i=$i+1
done

#DEPLOY
if [ "$id" != "" ]; then
	copy_shared
	dev_appserver.py "${yaml_out[@]}" --enable_sendmail --host=0.0.0.0
	clear_shared
fi

#CLEANUP
for injected in "${yaml_out[@]}"
do
	rm "$injected"
done