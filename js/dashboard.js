
$(document).ready(function(){

  // $("#phone").mask("(999) 999-9999");

  // Make it so text is selected when we click in text boxes.
  $("input:text")
        .focus(function () { $(this).select(); } )
        .mouseup(function (e) {e.preventDefault(); });
  // Same but for larger text fields
  $("textarea")
        .focus(function () { $(this).select(); } )
        .mouseup(function (e) {e.preventDefault(); });

  // Prevent 'enter' from submitting forms - stops the entire page getting submitted with 'enter.'
  $(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });
 
  $('#prefs_button').click(function () { 
    $('#prefs_modal').modal();  
    loadStats();
  });

  $(".btn_submit_order").click(function(){
    submitOrder();
  });
  
  $(".submit_preferences_button").click(function(){
    console.log("Submitting Preferences");
    document.getElementById("submit_preferences_loading").innerHTML = "<img src='img/loading.gif'>";
    submitPreferences();
    
  });

  $(".reset_order_counter_button").click(function(){
    console.log("Resetting Order Counter");
    document.getElementById("reset_order_counter_loading").innerHTML = "<img src='img/loading.gif'>";
    resetOrderCounter();

  });

  loadOrderList();
  loadFinishedList();

});




function resetStats(){
  document.getElementById("stats_table").innerHTML = "<img src='img/loading.gif'>";
  $('#prefs_modal').modal();   

  $.ajax({
    type:"GET",
    url:"/resetStats",
    data:data,
    success:function(data){
      document.getElementById("stats_table").innerHTML = data;
    }
  });
}

function resetOrderCounter(){
  $('#prefs_modal').modal();   
  data = {}
  $.ajax({
    type:"POST",
    url:"/resetOrderCounter",
    data:data,
    success:function(data){
      document.getElementById("reset_order_counter_loading").innerHTML = "";
    }
  });
}



function submitPreferences(){
  var custom_message = document.getElementById('custom_message').value;
  custom_message = encodeURIComponent(custom_message);
  console.log("Submitting preferences");
  console.log(custom_message);
  data = {
    custom_message:custom_message
  }
  $.ajax({
    type:"POST",
    url:"/submitPreferences",
    data:data,
    success:function(data){

        document.getElementById("submit_preferences_loading").innerHTML = "Saved.";
        console.log("fading out")
        // $(".submit_preferences_loading").fadeOut(4000);
        // document.getElementById("submit_preferences_loading").fadeOut();
        console.log("faded");
        document.getElementById("submit_preferences_loading").innerHTML = "";

    }
  });
}





function submitOrder(){
  console.log("Submitting order");

  

  var mobile_number = $(".mobile_number_input").val().replace(/\s/g, "");
  if(mobile_number.length != 10){
    document.getElementById("input_status").innerHTML = "Please enter a standard 10-digit Australian mobile phone number.";
  }else{

    document.getElementById("submit_status").innerHTML = "<img src='img/loading.gif'>";
    document.getElementById("input_status").innerHTML = "";
    document.getElementById("mobile_number_input").value = "";
    data = {
      'mobile_number':mobile_number,
      'date':'date',
      'location':'location'
    }

    $.ajax({
      type:"POST",
      url:"/submitOrder",
      data:data,
      success:function(){
        loadOrderList();
        $(document.body).animate({'scrollTop':$('#order_list_section').offset().top }, 400);
        document.getElementById("submit_status").innerHTML = "";
      }
    });
  }

}


function loadStats(){
  document.getElementById("stats_table").innerHTML = "<img src='img/loading.gif'>";
  

  $.ajax({
    type:"GET",
    url:"/stats",
    data:data,
    success:function(data){
      document.getElementById("stats_table").innerHTML = data;
    }
  });
}

function loadOrderList(){
  console.log('updating order list');
  data = {}
  $.ajax({
    type:"GET",
    url:"/orderList",
    data:data,
    success:function(data){
      console.log('order list retrieved');
      // console.log(data)
      document.getElementById("order_list").innerHTML = data;
    }
  });
}

function loadFinishedList(){
  console.log('updating order list');
  data = {}
  $.ajax({
    type:"GET",
    url:"/finishedList",
    data:data,
    success:function(data){
      console.log('finished list retrieved');
      // console.log(data)
      document.getElementById("finished_list").innerHTML = data;
    }
  });
}



function setDiv(element_id, string){
  var my_element = document.getElementById(element_id);
  if(my_element){
    my_element.innerHTML = string;
  }
}




function sendMessage(mobile_number, unique_order_id){
  setDiv("send_message_status"+unique_order_id, "<img src='img/loading.gif'>");
  setDiv("resend_message_status"+unique_order_id,"<img src='img/loading.gif'>");
  
  
  data = {'mobile_number':mobile_number, 'unique_order_id': unique_order_id};
  $.ajax({
    type:"POST",
    url:"/sendMessage",
    data:data,
    success:function(data){
      loadOrderList();
      loadFinishedList();
      setDiv("send_message_status"+unique_order_id , "");
      setDiv("resend_message_status"+unique_order_id , "");
    }
  });
}

function moveToFinishedList(mobile_number, unique_order_id){
  
  data = {'mobile_number':mobile_number, 'unique_order_id':unique_order_id};
  $.ajax({
      type:"POST",
      url:"/moveToFinishedList",
      data:data,
      success:function(data){
        loadOrderList();
        loadFinishedList();
        $(document.body).animate({'scrollTop':$('#finished_order_section').offset().top }, 400);
      }
  });
}






