var handler = StripeCheckout.configure({
    // key: 'pk_test_f24xlrXyUhSeZTyWyxbMiiCW',
    key: 'pk_live_PNiW15NUG1fFP4OKn2Qa3t8U',
    image: 'https://openmerchantaccount.com/img/ebl_logo_small.png',
    currency:'AUD',

    token: function(token) {
      // Use the token to create the charge with a server-side script.
      // You can access the token ID with `token.id`

      var data = {
        stripeToken:token.id,
        payment_email:token.email
      }

      $.ajax({
        type:"POST",
        url:"/payment",
        data:data,
        success:function(data){
          $('#prefs_modal').modal();   
          loadStats();
        }
      });
    }
  });

$('#start_checkout_button').on('click', function(e) {
  $('#prefs_modal').modal('hide');  
    // Open Checkout with further options
    handler.open({
      name: 'Buzz. Eat. Love.',
      description: '1000 Messages ($10 - 1c per message)',
      amount: 1000
    });
    e.preventDefault();
});

// Close Checkout on page navigation
$(window).on('popstate', function() {
    handler.close();
});