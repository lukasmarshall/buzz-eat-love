from google.appengine.ext import ndb
import datetime
import urllib
import models.order


class UserData(ndb.Model):
	user_object = ndb.UserProperty(required = True)
	total_order_counter = ndb.IntegerProperty(required=True)
	total_sent_messages_counter = ndb.IntegerProperty(required = True)
	remaining_messages_counter = ndb.IntegerProperty(required = True)
	custom_message = ndb.StringProperty(required=True)
	temporary_order_counter = ndb.IntegerProperty(required=True)
	account_type = ndb.StringProperty(required = True)
	user_created_date = ndb.DateTimeProperty(required = True)
	stats_start_date = ndb.DateTimeProperty(required = True)
	stripe_customer_id = ndb.StringProperty(required = False)

	def incrementOrderCounters(self):
		print "incrementing order counters"
		self.total_order_counter =self.total_order_counter+ 1
		self.temporary_order_counter = self.temporary_order_counter+ 1
		self.put()

	def incrementSentMessageCounters(self):
		self.total_sent_messages_counter = self.total_sent_messages_counter + 1
		self.remaining_messages_counter = max(self.remaining_messages_counter - 1,0)
		self.put()

	def getCurrentOrderNumber(self):
		return self.temporary_order_counter

	def resetCurrentOrderNumber(self):
		self.temporary_order_counter = 0
		self.put()

	def getCurrentOrderID(self):
		return self.total_order_counter

	def setStripeCustomerID(self, stripe_customer_id):
		self.stripe_customer_id = stripe_customer_id
		self.put()
		
	def getStripeCustomerID(self):
		if self.stripe_customer_id:
			return self.stripe_customer_id
		else:
			return None

	def getTurnAroundTime(self):
		ordersQuery = models.order.Order.query(
			models.order.Order.message_sent == True, 
			models.order.Order.date > self.stats_start_date, 
			ancestor=models.order.order_key(self.user_object.email())
			).fetch()
		cumulativeTime = 0
		counter = 0		
		for order in ordersQuery:
			timeDifference = order.first_message_date - order.date
			cumulativeTime = cumulativeTime + timeDifference.seconds
			counter = counter + 1
		if counter > 0:
			return round(float(cumulativeTime) / (float(counter) * float(60)), 2)
		else:
			return 0

	def getCustomerCollectTime(self):
		ordersQuery = models.order.Order.query(
			models.order.Order.message_sent == True, 
			models.order.Order.order_finished == True, 
			models.order.Order.date > self.stats_start_date, 
			ancestor=models.order.order_key(self.user_object.email())
			).fetch()
		cumulativeTime = 0
		counter = 0	
		for order in ordersQuery:
			timeDifference = order.order_collected_date - order.first_message_date
			cumulativeTime = cumulativeTime + timeDifference.seconds
			counter = counter + 1
		if counter > 0:
			return round(float(cumulativeTime) / (float(counter) * float(60)), 2)
		else:
			return 0

	def getOrdersLoggedSinceReset(self):
		ordersQuery = models.order.Order.query(
			models.order.Order.date > self.stats_start_date, 
			ancestor=models.order.order_key(self.user_object.email())
			).fetch()
		return len(ordersQuery)

	def resetStats(self):
		print "Resetting Stats"
		self.stats_start_date = datetime.datetime.now()
		self.put()

	def addMessages(self, num_messages):
		self.remaining_messages_counter = max(self.remaining_messages_counter + num_messages, 0)
		self.put()
	def setCustomMessage(self, custom_message):
		self.custom_message = custom_message
		self.put()
	def getUnescapedMessage(self):
		return urllib.unquote(self.custom_message)


def user_data_key(userEmail):
	return ndb.Key('user', userEmail)

def getUserData(user_object):
	userDataQuery = UserData.query(ancestor=user_data_key(user_object.email())).fetch()

	if len(userDataQuery) == 0:
		print "Creating New User"
		new_user_data = UserData(parent =user_data_key(user_object.email()), 
			user_object = user_object,
			total_order_counter = 0,
			total_sent_messages_counter = 0,
			remaining_messages_counter = 20,
			account_type = "Demo" ,
			custom_message = "Your order is ready for collection.",
			temporary_order_counter = 0,
			user_created_date = datetime.datetime.now(),
			stats_start_date = datetime.datetime.now()
			)
		new_user_data.put()
		user_data = new_user_data
	else:
		user_data = userDataQuery[0]
	return user_data




