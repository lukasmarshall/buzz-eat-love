from google.appengine.ext import ndb
import models.userData 
import datetime

class Order(ndb.Model):
	mobile_number = ndb.StringProperty(required = True)
	user = ndb.UserProperty(required = True)
	date = ndb.DateTimeProperty(required = True)

	first_message_date = ndb.DateTimeProperty(required=False)
	order_collected_date = ndb.DateTimeProperty(required=False)

	order_number = ndb.IntegerProperty(required = True)
	unique_order_id = ndb.IntegerProperty(required = True)
	order_finished = ndb.BooleanProperty(required = True)
	message_sent = ndb.BooleanProperty(required = True)

	def setMessageSent(self):
		print "Setting Message Sent"
		self.first_message_date = datetime.datetime.now()
		self.message_sent = True
		self.put()
	def setOrderFinished(self):
		print "Setting Order Finished"
		self.order_collected_date = datetime.datetime.now()
		self.order_finished = True
		self.put()

def order_key(userEmail):
	return ndb.Key('order', userEmail)

def getOrder(user, mobile_number, unique_order_id):
	print "getting Order "
	ordersQuery = Order.query(Order.unique_order_id == int(unique_order_id), ancestor=models.order.order_key(user.email())).order(-Order.date).fetch()
	if len(ordersQuery) > 0:
		print "order exists"
		order_object = ordersQuery[0]
	else:
		print "order does not exist. creating new order object."
		order_object = newOrder(user, mobile_number)

	return order_object

def newOrder(user, mobile_number):
	print "New Order Object : "+str(mobile_number)
	# Get the user's current data
	user_data = models.userData.getUserData(user)
	user_data.incrementOrderCounters()

	order_number = user_data.getCurrentOrderNumber()
	unique_order_id = user_data.getCurrentOrderID()
	
	print "Order Number "+str(order_number)
	print "ID: "+str(unique_order_id)
	order_object = Order(parent =models.order.order_key(user.email()), user = user, mobile_number =  mobile_number, date = datetime.datetime.now(), order_number = order_number, unique_order_id = unique_order_id, order_finished = False, message_sent = False)
	order_object.put()
	print "Object's Order Number "+str(order_object.order_number)
	print "Object's ID: "+str(order_object.unique_order_id)

	return order_object

