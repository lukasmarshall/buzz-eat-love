from google.appengine.ext import ndb
import datetime
import urllib2
import json

APP_KEY="eKQjt1A2SHyNzBfDDCPdTuNxJnVAQEa8"
APP_SECRET="a0suBWDAWiqAiGqi"

class Token(ndb.Model):
	secret = ndb.StringProperty(required = True)
	date = ndb.DateTimeProperty(required=True)

def token_key():
	return ndb.Key('token', 'telstraToken')

def refreshToken():
	tokens = Token.query(ancestor = token_key()).fetch()
	if len(tokens) > 0: #if a token exists in the database,
		token = tokens[0] 
		td = datetime.timedelta(hours = 1)
		if datetime.datetime.now() > (token.date + td): #check if the token has expired, if so get a new one
			token.date = datetime.datetime.now()
			token.secret = fetchNewSecret()
			token.put()
	else:
		token = Token(parent=token_key(), secret = fetchNewSecret(), date = datetime.datetime.now())
		token.put()

def fetchNewSecret():
	url =  "https://api.telstra.com/v1/oauth/token?client_id="+APP_KEY+"&client_secret="+APP_SECRET+"&grant_type=client_credentials&scope=SMS"
	result = json.load(urllib2.urlopen(url))
	print result['access_token']
	return result['access_token']

# Call this any time the token is needed - it will take care of everything.
def getToken():
	refreshToken()
	token = Token.query(ancestor = token_key()).fetch()[0]
	return token.secret
