#!/bin/sh

#SANITY CHECK
read -p "You are about to deploy. If you're sure, type 'YEPALLG'. " ans
case $ans in
	YEPALLG ) echo "Ok."; break;;
	* ) echo "Cancelling deployment."; exit;;
esac

#DECLARATIONS
id=$(jq -r '.APP_ID' $1)

yaml="app.yaml"
yaml_out="app-injected.yaml"

#INJECT SECRETS INTO YAML
add_secrets () {
	injected=`cat $2`
	injected="$injected"$'\n'$'\n'"env_variables:"

	secrets=$(jq -r 'to_entries | .[] | .[]' $1)
	while read key
		do
			read val
			injected="$injected"$'\n'"  $key: $val"
		done <<< "$secrets"
}

add_secrets $1 $yaml
echo "$injected" | cat > $yaml_out

#DEPLOY
if [ "$id" != "" ]; then
	appcfg.py update_cron . -A "$id"
	appcfg.py update_queues . -A "$id"
	appcfg.py update_indexes . -A "$id"
	appcfg.py update "$yaml_out" -A "$id"
fi

#CLEANUP
rm "$yaml_out"
