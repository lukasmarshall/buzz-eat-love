#!/bin/sh

#DECLARATIONS
id=$(jq -r '.APP_ID' $1)

yaml="app.yaml"
yaml_out="app-injected.yaml"

#INJECT SECRETS INTO YAML
add_secrets () {
	injected=`cat $2`
	injected="$injected"$'\n'$'\n'"env_variables:"

	secrets=$(jq -r 'to_entries | .[] | .[]' $1)
	while read key
		do
			read val
			injected="$injected"$'\n'"  $key: $val"
		done <<< "$secrets"
}

add_secrets $1 $yaml
echo "$injected" | cat > $yaml_out

#DEPLOY
if [ "$id" != "" ]; then
	dev_appserver.py "${yaml_out[@]}" --enable_sendmail --host=0.0.0.0
fi

#CLEANUP
rm "$yaml_out"
