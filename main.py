import webapp2
from handlers.mainhandler import MainHandler
from handlers.dashboardhandler import DashboardHandler
from handlers.submitorderhandler import SubmitOrderHandler
from handlers.orderlisthandler import OrderListHandler
from handlers.sendmessagehandler import SendMessageHandler
from handlers.movetofinishedlisthandler import MoveToFinishedListHandler
from handlers.finishedlisthandler import FinishedListHandler
from handlers.preferenceshandler import PreferencesHandler
from handlers.paymenthandler import PaymentHandler
from handlers.statshandler import StatsHandler
from handlers.resetstatshandler import ResetStatsHandler
from handlers.resetordercounterhandler import ResetOrderCounterHandler
from handlers.logouthandler import LogoutHandler
from handlers.logoutsuccesshandler import LogoutSuccessHandler

handlers = [
	('/', MainHandler),
	('/dashboard', DashboardHandler),
	('/submitOrder', SubmitOrderHandler),
	('/orderList', OrderListHandler),
	('/sendMessage', SendMessageHandler),
	('/moveToFinishedList',MoveToFinishedListHandler),
	('/finishedList', FinishedListHandler),
	('/submitPreferences', PreferencesHandler),
	('/payment', PaymentHandler),
	('/stats', StatsHandler),
	('/resetStats', ResetStatsHandler),
	('/resetOrderCounter', ResetOrderCounterHandler),
	('/logout', LogoutHandler),
	('/logoutSuccess', LogoutSuccessHandler)

	# If args are to be processed
	# (r'/api/<user:[^/]+>/<thing:[^/]+>', MyArgHandler)
]

routes = []
for handler in handlers:
	route = webapp2.Route(handler[0], handler=handler[1])
	routes.append(route)

app = webapp2.WSGIApplication(routes, debug=True)