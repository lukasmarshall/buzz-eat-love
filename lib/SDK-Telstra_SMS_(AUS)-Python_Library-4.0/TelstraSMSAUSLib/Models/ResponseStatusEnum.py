"""
TelstraSMSAUSLib

This file was automatically generated by APIMATIC BETA v2.0 on 06/12/2015
"""

class ResponseStatusEnum(object):

    """Implementation of the 'Response Status' enum.

    Enums to represent the status of a message request

    Attributes:
        PEND: The message is pending and has not yet been sent to the intended
            recipient
        SENT: The message has been sent to the intended recipient, but has not
            been delivered yet
        DELIVRD: The message has been delivered to the intended recipient
        READ: The message has been read by intended recipient and the
            recipient's response has been received

    """

    PEND = "PEND"

    SENT = "SENT"

    DELIVRD = "DELIVRD"

    READ = "READ"

