import os
import urllib

import jinja2
import webapp2

JINJA = jinja2.Environment(
	loader = jinja2.FileSystemLoader(os.path.dirname(__file__)),
	extensions = ['jinja2.ext.autoescape'],
	autoescape = True
)

class Templates(object):
	@staticmethod
	def load(template, template_values = {}):
		template = JINJA.get_template(template)
		return template.render(template_values)